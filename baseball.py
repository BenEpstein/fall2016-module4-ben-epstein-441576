from __future__ import division
import re
import operator
import sys, os

if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])

filename = sys.argv[1]

if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])

names = []
scores = []
dict = {}

def updateAverage(name):
    loc = names.index(name)
    (newBats,newHits) = scores[loc]
    newBats += bats
    newHits += hits
    tup = (newBats, newHits)
    scores[loc] = tup


def getAverage():
    tup = (bats, hits)
    scores.append(tup)


def getName():
    if names:
        if name in names:
            updateAverage(name)
        else:
            names.append(name)
            getAverage()
    else:
        names.append(name)
        getAverage()


with open(filename) as info:
    for line in info:
        info_re = '([\w\s]+)\sbatted\s(\d)\stimes with\s(\d)'
        words = re.match(info_re, line)
        if words:
            name = words.group(1)
            bats = (int)(words.group(2))
            hits = (int) (words.group(3))
            getName()

for num in range(0,len(scores)):
    (a,b) = scores.__getitem__(num)
    c = "%.3f" % round(b/a,3)
    person = names.__getitem__(num)
    dict.update({person: c})

x = sorted(dict.items(), key=operator.itemgetter(1))
x.reverse()
# for num in range(0,len(scores)):
#     (a,b) = x[num]
#     print(a, ":", b)
for k in x:
    print(k)
info.close()