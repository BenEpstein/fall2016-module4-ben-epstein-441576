# Baseball Stats Counter#

The St. Louis Cardinals are the most legendary baseball team in the national league. In this exercise, you will be creating a Python script that reads box scores from a file and computes the Cardinals' players' batting averages in a particular season.

##Tips and Instructions##

* You should write a Python script file to solve this problem.
* You should use a regular expression to parse players' names, at-bats, hits, and runs from the input file.
* You may want to create a class to hold and compute information about each player.
* Your file should take one command-line argument: the path to an input file. If no path is given, your program should print a usage message.